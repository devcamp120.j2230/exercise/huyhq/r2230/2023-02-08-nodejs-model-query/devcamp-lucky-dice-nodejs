const { default: mongoose } = require("mongoose");

const prizeModel = require("../model/Prize");

const createPrize = (req, res) => {
    let body = req.body;
    
    if (!body.name) {
        return res.status(400).json({
            message: `Error 400: Name phai bat buoc!`
        })
    }

    if (!body.description) {
        return res.status(400).json({
            message: `Error 400: Description phai bat buoc!`
        })
    }

    const newPrize = new prizeModel({
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
    })

    prizeModel.create(newPrize, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(201).json({
                message: `Create Prize successfull!`,
                Prize: data
            })
        }
    })
};

const getAllPrize = (req, res) => {
    prizeModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Load data successfull!`,
                Prize: data
            })
        }
    })
};

const getPrizeById = (req, res) => {
    let prizeId = req.params.prizeId;

    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            message: `Error 400: Id Prize khong dung!`
        })
    }

    prizeModel.findById(prizeId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Load data successfull!`,
                Prize: data
            })
        }
    })
};

const updatePrizeById = (req, res) => {
    let prizeId = req.params.prizeId;

    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            message: `Error 400: Id Prize khong dung!`
        })
    }

    let body = req.body;
    if (!body.name) {
        return res.status(400).json({
            message: `Error 400: Name phai bat buoc!`
        })
    }

    if (!body.description) {
        return res.status(400).json({
            message: `Error 400: Description phai bat buoc!`
        })
    }

    const prize = new prizeModel({
        name: body.name,
        description: body.description,
    })

    prizeModel.findByIdAndUpdate(prizeId, prize, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Update Prize successfull!`,
                Prize: data
            })
        }
    })
};
const deletePrizeById = (req, res) => {
    let prizeId = req.params.prizeId;

    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            message: `Error 400: Id Prize khong dung!`
        })
    }

    prizeModel.findByIdAndDelete(prizeId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(204).json({
                message: `Delete Prize successfull!`,
                Prize: data
            })
        }
    })
};

module.exports = {
    createPrize,
    getAllPrize,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
}