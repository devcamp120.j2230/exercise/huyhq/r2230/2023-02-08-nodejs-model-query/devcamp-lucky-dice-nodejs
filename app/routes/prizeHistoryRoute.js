const express = require("express");

const {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById
} = require("../controller/prizeHistoryController");

const prizeHistoryRouter = express.Router();

prizeHistoryRouter.get("/prize-history", getAllPrizeHistory);
prizeHistoryRouter.post("/prize-history", createPrizeHistory);
prizeHistoryRouter.get("/prize-history/:prizeHistoryId", getPrizeHistoryById);
prizeHistoryRouter.put("/prize-history/:prizeHistoryId", updatePrizeHistoryById);
prizeHistoryRouter.delete("/prize-history/:PrizeHistoryId", deletePrizeHistoryById);

module.exports = {prizeHistoryRouter};