const express = require("express");

const {
    createPrize,
    getAllPrize,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
} = require("../controller/prizeController");

const prizeRouter = express.Router();

prizeRouter.get("/prize", getAllPrize);
prizeRouter.post("/prize", createPrize);
prizeRouter.get("/prize/:prizeId", getPrizeById);
prizeRouter.put("/prize/:prizeId", updatePrizeById);
prizeRouter.delete("/prize/:prizeId", deletePrizeById);

module.exports = {prizeRouter};